package com.moveinmed.pro.controller;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.moveinmed.pro.exception.ResourceNotFoundException;
import com.moveinmed.pro.model.Professional;
import com.moveinmed.pro.repository.ProfessionalRepository;

@RestController
@RequestMapping("/v1")
public class ProfessionalController {
	
	@Autowired
	private ProfessionalRepository professionalRepository;
	
	@GetMapping("/professionals")
	public List<Professional> getAllProfessionals(
			@RequestParam(value = "lastname", required = false) String lastName,
            @RequestParam(value = "profession", required = false) String profession, 
            @RequestParam(value = "sort", required = false) String sort){

		if(lastName == null && profession == null) {
			List<Professional> pros = (List<Professional>) professionalRepository.findAll();
			this.sortProfessionals(pros, sort);
			return pros;
		} else if(lastName != null && profession == null){
			List<Professional> pros = (List<Professional>) professionalRepository.findByLastName(lastName); 
			this.sortProfessionals(pros, sort);
			return pros; 
		} else if(lastName == null && profession != null){
			List<Professional> pros = (List<Professional>) professionalRepository.findByProfession(profession); 
			this.sortProfessionals(pros, sort);
			return pros;
		} else {
			List<Professional> pros = (List<Professional>) professionalRepository.findByLastNameAndProfession(lastName, profession); 
			this.sortProfessionals(pros, sort);
			return pros; 
		}
	}
	
	private List<Professional> sortProfessionals(List<Professional> pros, String sort){
		sort = sort.toLowerCase();
		switch(sort) {
		case "lastname":
			pros.sort(Comparator.comparing(Professional::getLastName));
			return pros;
		case "firstname":
			pros.sort(Comparator.comparing(Professional::getFirstName));
			return pros;
		case "profession":
			pros.sort(Comparator.comparing(Professional::getProfession));
			return pros;
		case "id":
			pros.sort(Comparator.comparing(Professional::getId));
			return pros;
		default:
			return pros;
		}
	}
	
	@GetMapping("/professionals/{id}")
	Optional<Professional> getDetailsProfessional(@PathVariable Long id) {
		Optional<Professional> pro = professionalRepository.findById(id);
	    return pro;
	   }
	
   @PostMapping("/professionals")
   public Professional postProfessional(@RequestBody Professional newProfessional) {
     return professionalRepository.save(newProfessional);
   }
   
   @PutMapping("/professionals/{id}")
   Professional replaceProfessional(@RequestBody Professional newProfessional, @PathVariable Long id) {

     return professionalRepository.findById(id)
       .map(professional -> {
    	   professional.setFirstName(newProfessional.getFirstName());
    	   professional.setLastName(newProfessional.getLastName());
    	   professional.setPhoneNumber(newProfessional.getPhoneNumber());
    	   professional.setEmail(newProfessional.getEmail());
    	   professional.setAddress(newProfessional.getAddress());
    	   professional.setProfession(newProfessional.getProfession());
         return professionalRepository.save(professional);
       })
       .orElseGet(() -> {
    	   newProfessional.setId(id);
         return professionalRepository.save(newProfessional);
       });
   }
   
   @DeleteMapping("/professionals/{id}")
   ResponseEntity<?> deleteEmployee(@PathVariable Long id) {

	   professionalRepository.deleteById(id);

     return ResponseEntity.noContent().build();
   }
 	

}
