# REST API with Spring Boot for Move In Med test

## Context

We have a database holding data about professionals. We want to get informations about those professionals, but we want to be able to create, update and delete those professionals entities.

## Methods

The full documentation about endpoints can be found here : https://web.postman.co/collections/9168439-87185a98-0f1d-405f-991a-d4d4a3424f48?version=latest&workspace=8df7da2a-03cd-4aed-8c2e-3a2c9a99fd9d

| Method | GET | POST | PUT | DELETE |
| ------ | ------ | ------ | ------ | ------ |
| /professionals | List all professionals | Create a professional | - | - | 
| /professionals/{id} | Return a single professional | - | Update a professional | Delete a professional | 


## Request and response examples

### /GET

Request :
`http://localhost:8080/v1/professionals`

Response : 
```json
[
   {
      "id":1,
      "lastName":"postUser",
      "firstName":"postUser",
      "email":"postUser@gmail.com",
      "phoneNumber":"01.01.01.01.01",
      "address":"1 rue du numero un 11111 Un-City",
      "profession":"Infirmier"
   },
   {
      "id":2,
      "lastName":"putUser",
      "firstName":"putUser",
      "email":"postUser@gmail.com",
      "phoneNumber":"01.01.01.01.01",
      "address":"1 rue du numero un 11111 Un-City",
      "profession":"Infirmier"
   },
   {
      "id":3,
      "lastName":"John",
      "firstName":"Doe",
      "email":"john.doe@gmail.com",
      "phoneNumber":"01.01.01.01.01",
      "address":"1 rue du numero un 11111 Un-City",
      "profession":"Chirurgien"
   }
]
```

Response code : 200


Request :
`http://localhost:8080/v1/professionals/1`

Response : 
```json
{
  "id":1,
  "lastName":"postUser",
  "firstName":"postUser",
  "email":"postUser@gmail.com",
  "phoneNumber":"01.01.01.01.01",
  "address":"1 rue du numero un 11111 Un-City",
  "profession":"Infirmier"
}
```

Response code : 200

### /POST

Request :
`http://localhost:8080/v1/professionals`

Body :
```json
{
        "lastName": "postUser",
        "firstName": "postUser",
        "email": "postUser@gmail.com",
        "phoneNumber": "01.01.01.01.01",
        "address": "1 rue du numero un 11111 Un-City",
        "profession": "Infirmier"
}

```

Response code : 201

### /PUT

Request :
`http://localhost:8080/v1/professionals/2`

Body :
```json
{
        "lastName": "putUser",
        "firstName": "putUser",
        "email": "putUser@gmail.com",
        "phoneNumber": "01.01.01.01.01",
        "address": "1 rue du numero un 11111 Un-City",
        "profession": "Infirmier"
}
```

Response code : 200 or 204


### /DELETE

Request :
`http://localhost:8080/v1/professionals/2`

Response code : 204
