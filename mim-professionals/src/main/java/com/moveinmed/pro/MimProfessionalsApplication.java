package com.moveinmed.pro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MimProfessionalsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MimProfessionalsApplication.class, args);
	}

}
