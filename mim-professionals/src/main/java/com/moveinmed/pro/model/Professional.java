package com.moveinmed.pro.model;

import javax.persistence.*;
import javax.validation.constraints.Email;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;


//Professional entity corresponding to our table Professionals in our PostGreSQL database.
@Entity
@Table(name = "professionals")
@EntityListeners(AuditingEntityListener.class)
public class Professional {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ProfessionalID", nullable = false)
	private long id;

    @Column(name = "Firstname", nullable = false)
	private String lastName;

    @Column(name = "Lastname", nullable = false)
	private String firstName;

    @Column(name = "Email", nullable = false)
	@Email(message = "Enter a valid email please")
	private String email;
    
    @Column(name = "Phonenumber", nullable = false)
	private String phoneNumber;

    @Column(name = "Address", nullable = false)
	private String address;

    @Column(name = "Profession", nullable = false)
	private String profession;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}


}
