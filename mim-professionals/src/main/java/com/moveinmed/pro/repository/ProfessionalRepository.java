package com.moveinmed.pro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moveinmed.pro.model.Professional;

@Repository
public interface ProfessionalRepository extends CrudRepository<Professional, Long> {
	List<Professional> findByProfession(String profession);
	List<Professional> findByLastNameContaining(String lastName);
	List<Professional> findByLastNameAndProfessionContaining(String lastName, String profession);
	Professional findById(long id);
	
}